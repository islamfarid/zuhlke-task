package com.example.zuhlketask.traffics.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.cartrackusers.MainCoroutineRule
import com.example.cartrackusers.testObserver
import com.example.zuhlketask.common.Mapper
import com.example.zuhlketask.common.UseCase
import com.example.zuhlketask.traffics.models.domain.Camera
import com.example.zuhlketask.traffics.models.domain.Location
import com.example.zuhlketask.traffics.models.ui.CameraUiModel
import com.example.zuhlketask.traffics.models.ui.LocationUiModel
import com.example.zuhlketask.traffics.models.ui.TrafficsUiState
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class TrafficsViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()
    private lateinit var viewModel: TrafficsViewModel

    private val gettingTrafficsUseCase: UseCase<Unit, List<Camera>> = mockk()
    private val cameraUiMapper: Mapper<Camera, CameraUiModel> = mockk()

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        viewModel = TrafficsViewModel(
            gettingTrafficsUseCase = gettingTrafficsUseCase,
            cameraUiMapper = cameraUiMapper,
            coroutineDispatcher = mainCoroutineRule.testDispatcher
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `GIVEN traffics loaded  WHEN Map Activity created THEN screen render data`() =
        mainCoroutineRule.runBlockingTest {
            val uiStateObserver = viewModel.uiState.testObserver()

            val apiData = listOf(
                Camera(
                    image = "test_image",
                    location = Location(latitude = 2.3, longitude = 3.6),
                    timestamp = "timestamp"
                )
            )
            val cameraUiModel = CameraUiModel(
                image = "test_image",
                locationUiModel = LocationUiModel(latitude = 2.3, longitude = 3.6),
                timestamp = "timestamp"
            )
            coEvery { gettingTrafficsUseCase.execute(any()) } returns apiData
            coEvery { cameraUiMapper.map(any()) } returns cameraUiModel

            viewModel.onViewCreated()

            coVerify { gettingTrafficsUseCase.execute(Unit) }
            coVerify { cameraUiMapper.map(apiData[0]) }

            TestCase.assertEquals(uiStateObserver.observedValues.first(), TrafficsUiState.Loading)
            val trafficLoaded: List<CameraUiModel> =
                (uiStateObserver.observedValues.last() as TrafficsUiState.TrafficsLoaded).cameraUiModels
            TestCase.assertEquals(
                trafficLoaded,
                listOf(cameraUiModel)
            )
        }

    @ExperimentalCoroutinesApi
    @Test
    fun `GIVEN traffics loading error  WHEN Map Activity created THEN screen error is displayed`() =
        mainCoroutineRule.runBlockingTest {
            val uiStateObserver = viewModel.uiState.testObserver()

            coEvery { gettingTrafficsUseCase.execute(any()) } throws Throwable()

            viewModel.onViewCreated()

            coVerify { gettingTrafficsUseCase.execute(Unit) }

            TestCase.assertEquals(uiStateObserver.observedValues.first(), TrafficsUiState.Loading)
            val error = uiStateObserver.observedValues.last()
            TestCase.assertEquals(
                error,
                TrafficsUiState.Error
            )
        }

}