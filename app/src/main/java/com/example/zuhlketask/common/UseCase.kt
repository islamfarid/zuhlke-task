package com.example.zuhlketask.common

interface UseCase<Params, Result> {
    suspend fun execute(params: Params?): Result
}