package com.example.zuhlketask.common

interface Mapper<Params, Result> {
    suspend fun map(from: Params): Result
}