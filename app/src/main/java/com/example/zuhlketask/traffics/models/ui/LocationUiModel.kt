package com.example.zuhlketask.traffics.models.ui


data class LocationUiModel(
    val latitude: Double,
    val longitude: Double
)