package com.example.zuhlketask.traffics.repositories

import com.example.zuhlketask.traffics.models.domain.Camera

interface TrafficRepository {
    suspend fun fetchTraffics(): List<Camera>
}