package com.example.zuhlketask.traffics.di

import com.example.zuhlketask.common.Mapper
import com.example.zuhlketask.common.UseCase
import com.example.zuhlketask.di.IOCoroutineDispatcher
import com.example.zuhlketask.traffics.api.TrafficApi
import com.example.zuhlketask.traffics.api.models.CameraApiModel
import com.example.zuhlketask.traffics.datasource.TrafficsDataSource
import com.example.zuhlketask.traffics.datasource.TrafficsDataSourceImpl
import com.example.zuhlketask.traffics.mappers.CameraApiMapper
import com.example.zuhlketask.traffics.mappers.CameraUiMapper
import com.example.zuhlketask.traffics.mappers.MapperCameraApi
import com.example.zuhlketask.traffics.mappers.MapperCameraUi
import com.example.zuhlketask.traffics.models.domain.Camera
import com.example.zuhlketask.traffics.models.ui.CameraUiModel
import com.example.zuhlketask.traffics.repositories.TrafficRepository
import com.example.zuhlketask.traffics.repositories.TrafficRepositoryImpl
import com.example.zuhlketask.traffics.usecases.GettingTrafficsUseCase
import com.example.zuhlketask.traffics.usecases.UseCaseGettingTraffics
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
abstract class TrafficsModule {
    @Binds
    @Reusable
    abstract fun bindTrafficsDataSource(trafficsDataSource: TrafficsDataSourceImpl): TrafficsDataSource

    @Binds
    @Reusable
    abstract fun bindTrafficRepository(trafficRepositoryImpl: TrafficRepositoryImpl): TrafficRepository

    @Binds
    @MapperCameraApi
    abstract fun bindCameraApiMapper(cameraApiMapper: CameraApiMapper): Mapper<CameraApiModel, Camera>

    @Binds
    @MapperCameraUi
    abstract fun bindCountriesUiMapper(cameraUiMapper: CameraUiMapper): Mapper<Camera, CameraUiModel>

    companion object {
        @Provides
        @Reusable
        fun provideTrafficsApi(retrofit: Retrofit): TrafficApi =
            retrofit.create(TrafficApi::class.java)

        @Provides
        @UseCaseGettingTraffics
        fun provideGettingTrafficsUseCase(
            trafficRepository: TrafficRepository,
            @IOCoroutineDispatcher coroutineDispatcher: CoroutineDispatcher
        ): UseCase<Unit, List<Camera>> {
            return GettingTrafficsUseCase(trafficRepository, coroutineDispatcher)
        }

    }
}