package com.example.zuhlketask.traffics.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import coil.load
import com.example.zuhlketask.databinding.FragmentCameraImageBinding

class CameraImageDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentCameraImageBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCameraImageBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.apply { binding.cameraImageView.load(getString(CAMERA_IMAGE)) }

    }

    companion object {
        const val CAMERA_IMAGE = "camera_image"
        val TAG = CameraImageDialogFragment::class.simpleName

        fun newInstance(cameraUrl: String): CameraImageDialogFragment {
            val args = Bundle().apply { putString(CAMERA_IMAGE, cameraUrl) }
            val fragment = CameraImageDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }
}