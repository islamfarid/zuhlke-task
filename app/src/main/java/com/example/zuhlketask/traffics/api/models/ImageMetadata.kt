package com.example.zuhlketask.traffics.api.models


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ImageMetadata(
    @SerialName("height")
    val height: Int,
    @SerialName("md5")
    val md5: String,
    @SerialName("width")
    val width: Int
)