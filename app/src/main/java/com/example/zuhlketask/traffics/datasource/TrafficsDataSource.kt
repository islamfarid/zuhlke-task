package com.example.zuhlketask.traffics.datasource

import com.example.zuhlketask.traffics.api.models.CameraApiModel
import com.example.zuhlketask.traffics.api.models.TrafficItemApiModel

interface TrafficsDataSource {
    suspend fun fetchTraffics(): List<CameraApiModel>
}