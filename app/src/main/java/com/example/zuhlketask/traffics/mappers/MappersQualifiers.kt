package com.example.zuhlketask.traffics.mappers

import javax.inject.Qualifier

@Qualifier
annotation class MapperCameraApi

@Qualifier
annotation class MapperCameraUi