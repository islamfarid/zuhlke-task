package com.example.zuhlketask.traffics.models.ui

data class CameraUiModel(
    val image: String,
    val locationUiModel: LocationUiModel,
    val timestamp: String
)