package com.example.zuhlketask.traffics.mappers

import com.example.zuhlketask.common.Mapper
import com.example.zuhlketask.traffics.api.models.CameraApiModel
import com.example.zuhlketask.traffics.models.domain.Camera
import com.example.zuhlketask.traffics.models.domain.Location
import javax.inject.Inject

class CameraApiMapper @Inject constructor() : Mapper<CameraApiModel, Camera> {
    override suspend fun map(from: CameraApiModel): Camera {
        return Camera(
            image = from.image,
            location = Location(
                latitude = from.locationApiModel.latitude,
                longitude = from.locationApiModel.longitude
            ),
            timestamp = from.timestamp
        )
    }
}