package com.example.zuhlketask.traffics.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zuhlketask.common.Mapper
import com.example.zuhlketask.common.UseCase
import com.example.zuhlketask.di.DefaultCoroutineDispatcher
import com.example.zuhlketask.traffics.mappers.MapperCameraUi
import com.example.zuhlketask.traffics.models.domain.Camera
import com.example.zuhlketask.traffics.models.ui.CameraUiModel
import com.example.zuhlketask.traffics.models.ui.TrafficsUiState
import com.example.zuhlketask.traffics.usecases.UseCaseGettingTraffics
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class TrafficsViewModel @Inject constructor(
    @UseCaseGettingTraffics private val gettingTrafficsUseCase: UseCase<Unit, List<Camera>>,
    @MapperCameraUi private val cameraUiMapper: Mapper<Camera, CameraUiModel>,
    @DefaultCoroutineDispatcher private val coroutineDispatcher: CoroutineDispatcher,
) : ViewModel() {
    private val _uiState = MutableLiveData<TrafficsUiState>()
    val uiState: LiveData<TrafficsUiState> = _uiState
    fun onViewCreated() {
        viewModelScope.launch {
            _uiState.value = TrafficsUiState.Loading
            try {
                notifyTrafficsLoaded(gettingTrafficsUseCase.execute(Unit))
            } catch (throwable: Throwable) {
                _uiState.value = TrafficsUiState.Error
            }
        }
    }

    private suspend fun notifyTrafficsLoaded(usersList: List<Camera>) {
        _uiState.value = withContext(coroutineDispatcher) {
            TrafficsUiState.TrafficsLoaded(usersList.map { cameraUiMapper.map(it) })
        }
    }
}