package com.example.zuhlketask.traffics.api.models


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TrafficsApiModel(
    @SerialName("api_info")
    val apiInfo: ApiInfo,
    @SerialName("items")
    val trafficItemApiModels: List<TrafficItemApiModel>
)