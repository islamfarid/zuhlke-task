package com.example.zuhlketask.traffics.presentation

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.zuhlketask.R
import com.example.zuhlketask.databinding.ActivityMapBinding
import com.example.zuhlketask.traffics.models.ui.TrafficsUiState
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private val trafficsViewModel: TrafficsViewModel by viewModels()
    private lateinit var binding: ActivityMapBinding
    private var googleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapBinding.inflate(layoutInflater)
        setContentView(binding.root)
        prepareMap()
        listenToChanges()
        trafficsViewModel.onViewCreated()
    }

    private fun prepareMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
    }

    private fun listenToChanges() {
        trafficsViewModel.uiState.observe(this) { uiState ->
            when (uiState) {
                is TrafficsUiState.TrafficsLoaded -> {
                    handleUsersLoaded(uiState)
                }
                is TrafficsUiState.Loading -> {
                    handleLoading()
                }
                is TrafficsUiState.Error -> {
                    applyErrorUi()
                }
            }
        }
    }

    private fun applyErrorUi() {
        binding.loading.isVisible = false
        Snackbar.make(binding.root, getString(R.string.something_went_wrong), Snackbar.LENGTH_SHORT)
            .show()
    }

    private fun handleUsersLoaded(uiState: TrafficsUiState.TrafficsLoaded) {
        binding.loading.isVisible = false

        googleMap?.apply {
            uiState.cameraUiModels.forEach {
                val position = LatLng(it.locationUiModel.latitude, it.locationUiModel.longitude)
                val marker = addMarker(MarkerOptions().position(position))
                marker.tag = it.image
                moveCamera(CameraUpdateFactory.newLatLng(position))
            }
            animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL));
            setOnMarkerClickListener(this@MapActivity)
        }
    }

    private fun handleLoading() {
        binding.loading.isVisible = true
        // later we might hide the error if there is error view
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        return marker?.let {
            CameraImageDialogFragment.newInstance(it.tag.toString())
                .show(supportFragmentManager, CameraImageDialogFragment.TAG)
            true
        } ?: false
    }

    companion object {
        const val ZOOM_LEVEL = 10.0f
    }
}