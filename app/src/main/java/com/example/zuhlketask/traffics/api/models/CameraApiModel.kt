package com.example.zuhlketask.traffics.api.models


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CameraApiModel(
    @SerialName("camera_id")
    val cameraId: String,
    @SerialName("image")
    val image: String,
    @SerialName("image_metadata")
    val imageMetadata: ImageMetadata,
    @SerialName("location")
    val locationApiModel: LocationApiModel,
    @SerialName("timestamp")
    val timestamp: String
)