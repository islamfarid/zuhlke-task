package com.example.zuhlketask.traffics.datasource

import com.example.zuhlketask.traffics.api.TrafficApi
import com.example.zuhlketask.traffics.api.models.CameraApiModel
import javax.inject.Inject

class TrafficsDataSourceImpl @Inject constructor(private val trafficApi: TrafficApi) :
    TrafficsDataSource {
    override suspend fun fetchTraffics(): List<CameraApiModel> {
        val cameraApiModels = mutableListOf<CameraApiModel>()
        val body = trafficApi.fetchCountries().body()
        return body?.let {
            it.trafficItemApiModels.forEach { trafficItem ->
                cameraApiModels.addAll(
                    trafficItem.cameraApiModels
                )
            }
            cameraApiModels
        } ?: error(SOMETHING_WENT_WRONG)

    }

    companion object {
        const val SOMETHING_WENT_WRONG =
            "SOMETHING WENT WRONG" // Just as a simple as no error handler component
    }
}