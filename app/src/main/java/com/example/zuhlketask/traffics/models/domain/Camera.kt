package com.example.zuhlketask.traffics.models.domain


data class Camera(
    val image: String,
    val location: Location,
    val timestamp: String
)