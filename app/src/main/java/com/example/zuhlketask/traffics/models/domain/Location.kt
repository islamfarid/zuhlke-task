package com.example.zuhlketask.traffics.models.domain


data class Location(
    val latitude: Double,
    val longitude: Double
)