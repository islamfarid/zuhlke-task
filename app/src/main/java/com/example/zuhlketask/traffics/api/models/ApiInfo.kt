package com.example.zuhlketask.traffics.api.models


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ApiInfo(
    @SerialName("status")
    val status: String
)