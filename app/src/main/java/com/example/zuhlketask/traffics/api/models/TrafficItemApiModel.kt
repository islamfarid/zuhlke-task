package com.example.zuhlketask.traffics.api.models


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TrafficItemApiModel(
    @SerialName("cameras")
    val cameraApiModels: List<CameraApiModel>,
    @SerialName("timestamp")
    val timestamp: String
)