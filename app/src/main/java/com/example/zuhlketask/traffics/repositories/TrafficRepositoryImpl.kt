package com.example.zuhlketask.traffics.repositories

import com.example.zuhlketask.common.Mapper
import com.example.zuhlketask.traffics.api.models.CameraApiModel
import com.example.zuhlketask.traffics.datasource.TrafficsDataSource
import com.example.zuhlketask.traffics.mappers.MapperCameraApi
import com.example.zuhlketask.traffics.models.domain.Camera
import javax.inject.Inject

class TrafficRepositoryImpl @Inject constructor(
    private val trafficsDataSource: TrafficsDataSource,
    @MapperCameraApi private val cameraApiMapper: Mapper<CameraApiModel, Camera>
) : TrafficRepository {
    override suspend fun fetchTraffics(): List<Camera> {
       return trafficsDataSource.fetchTraffics().map { cameraApiMapper.map(it) }
    }
}