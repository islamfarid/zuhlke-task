package com.example.zuhlketask.traffics.api

import com.example.zuhlketask.traffics.api.models.TrafficsApiModel
import retrofit2.Response
import retrofit2.http.GET

interface TrafficApi {
    @GET("transport/traffic-images")
    suspend fun fetchCountries(): Response<TrafficsApiModel>
}