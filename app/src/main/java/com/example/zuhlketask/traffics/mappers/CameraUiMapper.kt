package com.example.zuhlketask.traffics.mappers

import com.example.zuhlketask.common.Mapper
import com.example.zuhlketask.traffics.models.domain.Camera
import com.example.zuhlketask.traffics.models.ui.CameraUiModel
import com.example.zuhlketask.traffics.models.ui.LocationUiModel
import javax.inject.Inject

class CameraUiMapper @Inject constructor() : Mapper<Camera, CameraUiModel> {
    override suspend fun map(from: Camera): CameraUiModel {
        return CameraUiModel(
            image = from.image,
            locationUiModel = LocationUiModel(
                latitude = from.location.latitude,
                longitude = from.location.longitude
            ),
            timestamp = from.timestamp
        )
    }
}