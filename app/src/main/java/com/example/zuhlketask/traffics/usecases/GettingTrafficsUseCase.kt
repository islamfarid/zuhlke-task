package com.example.zuhlketask.traffics.usecases

import com.example.zuhlketask.common.UseCase
import com.example.zuhlketask.di.IOCoroutineDispatcher
import com.example.zuhlketask.traffics.models.domain.Camera
import com.example.zuhlketask.traffics.repositories.TrafficRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GettingTrafficsUseCase @Inject constructor(
    private val trafficRepository: TrafficRepository,
    @IOCoroutineDispatcher private val CoroutineDispatcher: CoroutineDispatcher,
) : UseCase<Unit, List<Camera>> {
    override suspend fun execute(params: Unit?): List<Camera> = withContext(CoroutineDispatcher) {
        trafficRepository.fetchTraffics()
    }
}