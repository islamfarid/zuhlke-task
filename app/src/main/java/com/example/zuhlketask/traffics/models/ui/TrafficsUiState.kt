package com.example.zuhlketask.traffics.models.ui

sealed class TrafficsUiState {
    class TrafficsLoaded(val cameraUiModels: List<CameraUiModel>) : TrafficsUiState()
    object Error : TrafficsUiState()
    object Loading : TrafficsUiState()
}