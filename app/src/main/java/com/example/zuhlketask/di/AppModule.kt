package com.example.zuhlketask.di

import com.example.zuhlketask.traffics.api.TrafficApi
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

const val BASE_API_URL = "https://api.data.gov.sg/v1/"
const val CONTENT_TYPE = "application/json"

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {
    companion object {
        @Provides
        @Reusable
        @DefaultCoroutineDispatcher
        fun provideDefaultCoroutineDispatcher(): CoroutineDispatcher = Dispatchers.Default

        @Provides
        @Reusable
        @IOCoroutineDispatcher
        fun provideIOCoroutineDispatcher(): CoroutineDispatcher = Dispatchers.IO

        @ExperimentalSerializationApi
        @Provides
        @Singleton
        fun provideRetrofitClient(): Retrofit {
            val contentType = CONTENT_TYPE.toMediaType()
            val okHttpBuilder = OkHttpClient.Builder()
            val json = Json {
                ignoreUnknownKeys = true
                prettyPrint = true
            }.asConverterFactory(contentType)
            return Retrofit.Builder().baseUrl(BASE_API_URL)
                .addConverterFactory(json)
                .client(okHttpBuilder.build())
                .build()
        }
    }
}